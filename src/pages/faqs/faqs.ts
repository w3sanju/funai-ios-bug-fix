import { Component } from '@angular/core';
import { NavController, NavParams, } from 'ionic-angular';
import { ProductListProvider } from '../../providers/product-list/product-list';

@Component({
    selector: 'page-faqs',
    templateUrl: 'faqs.html',
})

export class FaqsPage {

    faqs :any;

    constructor(
        private productListProvider:ProductListProvider,
        public navCtrl: NavController,
        public navParams: NavParams
    ){

    }

    ngOnInit(){
        this.faqsData();
    }
    faqsData(){

          this.productListProvider.getProducts('faqs.json?num_id='+localStorage.getItem('prod_num_id')+'&thirdlevel_id='+localStorage.getItem('level_id3New')+'&lang=1')  .subscribe(success => {

            this.faqs = success.faqs.split('src="/media/').join('src="http://124.30.44.230/funaihelpver1//media/').split('<div><a href="#" onclick="window.close(); return false;">CLOSE</a></div>').join('');

            console.log(this.faqs);
          });
    }
    toggleSection(i) {
        this.faqs[i].open = !this.faqs[i].open;
    }
    toggleItem(i, j) {
        this.faqs[i].children[j].open = !this.faqs[i].children[j].open;
    }
}
